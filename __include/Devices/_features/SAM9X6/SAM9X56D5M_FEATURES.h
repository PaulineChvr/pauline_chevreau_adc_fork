/*----------------------------------------------------------------------------
 *         ATMEL Microcontroller Software Support  -  ROUSSET  -
 *----------------------------------------------------------------------------
 * The software is delivered "AS IS" without warranty or condition of any
 * kind, either express, implied or statutory. This includes without
 * limitation any warranty or condition with respect to merchantability or
 * fitness for any particular purpose, or against the infringements of
 * intellectual property rights of others.
 *----------------------------------------------------------------------------
 * File Name           : features_sam9x56xxx.h
 * Object              : ATSAM Definition File.
 *
 * Creation            : Feb/2018
 *----------------------------------------------------------------------------
 */
#ifndef _SAM9X56D5M_FEATURES_H
#define _SAM9X56D5M_FEATURES_H

// Add here features definitions that are specific to a particular CPN (not common to series)

#endif //_SAM9X56D5M_FEATURES_H
