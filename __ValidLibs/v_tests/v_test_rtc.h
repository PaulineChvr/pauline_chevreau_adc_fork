/**
 * \file v_test_rtc.h
 * \brief adc demo header file
 * \author AYi
 * \version 0.1
 * \date 01 Janvier 2016
 *
 */


#ifndef _V_TEST_ADC_H
#define _V_TEST_ADC_H

#include "project.h"

// ---- General purpose include files
#include "device.h"
#include "board.h"

// ---- Validation libraries
#include "v_debug.h"
#include "v_utils.h"
#include "v_drivers/v_pioV4.h"





/** 
 * \define RTC_MACRO_EXAMPLE 
 * \brief breif desc
 *
 *  Detailed desc
 *
 *  * \todo define ADC_TRACKING accurately 
 *  */
#define RTC_MACRO_EXAMPLE	15


#endif //_V_TEST_ADC_H
